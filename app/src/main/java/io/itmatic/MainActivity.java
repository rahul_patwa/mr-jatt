package io.itmatic;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import CommonClass.Resource;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private WebView browser;
    private TextView mWebsiteName,mCleanHistory;
    private boolean mBackPressed;
    private ImageView mPlay, mPause, mReplay;
    private boolean isplay;
    private MediaPlayer player;
    private RelativeLayout mPlayerlay;
    private ImageView mDownloadBtn, mHistoryBtn;
    private String mSongUrl;
    private Toolbar toolbar;
    private boolean hasplayer = true;
    private Menu mAllMenuItem;
    private NavigationView mNavView;
    private ProgressBar mProgress;
    private SharedPreferences.Editor editor;
    private ListView mHistoryList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        Resource.sSonglist = new ArrayList<>();

        mCleanHistory=(TextView)findViewById(R.id.btn_clean_history);
        mNavView = (NavigationView) findViewById(R.id.nav_view);
        mWebsiteName = (TextView) findViewById(R.id.website_name);
        mReplay = (ImageView) findViewById(R.id.replay);
        mProgress = (ProgressBar) findViewById(R.id.progress_bar);
        mPause = (ImageView) findViewById(R.id.pause);
        player = new MediaPlayer();
        mPlayerlay = (RelativeLayout) findViewById(R.id.player_lay);
        mDownloadBtn = (ImageView) findViewById(R.id.btn_download);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toolbar.inflateMenu(R.menu.menu_main);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        browser = (WebView) findViewById(R.id.webview);

        browser.loadUrl("http://www.geet.fm");


        mDownloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadsong(mSongUrl);
            }
        });

        mPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player.isPlaying()) {
                    player.pause();
                    mPause.setImageResource(R.drawable.c_play);

                } else {
                    player.start();
                    mPause.setImageResource(R.drawable.ic_pause);
                }

            }
        });

        mReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPause.setVisibility(View.VISIBLE);
                mReplay.setVisibility(View.GONE);
                playsong(mSongUrl);
            }
        });
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mPause.setVisibility(View.GONE);
                mReplay.setVisibility(View.VISIBLE);
            }
        });
        browser.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                Log.d("WEB_VIEW_TEST", "error code:" + errorCode + " - " + description);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.endsWith(".apk")) {

                } else if (url.endsWith(".mp3")) {
                    Resource.sSonglist.add(url);
                    editor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                    editor.putInt("Status_size", Resource.sSonglist.size());

                    for (int i = 0; i < Resource.sSonglist.size(); i++) {
                        editor.putString("Status_" + i, Resource.sSonglist.get(i));
                    }

                    editor.commit();
                    hasplayer = false;
                    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                            MainActivity.this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                    drawer.setDrawerListener(toggle);
                    supportInvalidateOptionsMenu();
                    toolbar.inflateMenu(R.menu.menu_main);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    toggle.syncState();
                    mSongUrl = url;
                    mPlayerlay.setVisibility(View.VISIBLE);
                    mPause.setImageResource(R.drawable.ic_pause);
                    playsong(url);

                }
                // if there is a link to anything else than .apk or .mp3 load the URL in the webview
                else if (url.endsWith(".mp4") || url.endsWith(".3gp") || url.endsWith(".avi")) {
                    downloadsong(url);
                } else view.loadUrl(url);
                return true;
            }
        });


    }


    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem register = menu.findItem(R.id.queue);

        if (hasplayer) {
            register.setVisible(false);
        } else {
            register.setVisible(true);
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.queue:
                mPlayerlay.setVisibility(View.VISIBLE);
                break;
            case R.id.websitequeue:
                inflateimage();
                break;

            case R.id.historyqueue:
                final SharedPreferences mSharedPreference1= MainActivity.this.getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
                Resource.sSonglist.clear();
                int size = mSharedPreference1.getInt("Status_size", 0);
                for (int i = 0; i < size; i++) {
                    Resource.sSonglist.add(mSharedPreference1.getString("Status_" + i, null));
                }
                showhistory();

                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (!mBackPressed) {
            Toast.makeText(this, "Press Again For Exit", Toast.LENGTH_LONG).show();
            mBackPressed = true;
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:

                    mPlayerlay.setVisibility(View.GONE);

                    if (browser.canGoBack()) {
                        browser.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.Blugaa:
                browser.loadUrl("https://www.blugaa.com/");
                mWebsiteName.setText(item.getTitle());
                break;
            case R.id.PenduJatt:
                mWebsiteName.setText(item.getTitle());
                browser.loadUrl("http://pendujatt.co.in/");
                break;
            case R.id.nav_DjPunjab:
                browser.loadUrl("http://djpunjab.com/");
                mWebsiteName.setText(item.getTitle());
                break;
            case R.id.Mr_Johal:
                browser.loadUrl("http://mr-johal.com/");
                mWebsiteName.setText(item.getTitle());
                break;
            case R.id.Geet_Fm:
                browser.loadUrl("http://www.geet.fm");
                mWebsiteName.setText(item.getTitle());
                break;
            case R.id.nav_vipJatt:
                browser.loadUrl("http://vipjatt.net/");
                mWebsiteName.setText(item.getTitle());
                break;

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void downloadsong(String url) {

        final File destinationDir = new File(Environment.getExternalStorageDirectory(), getPackageName());
        final DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse(url));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        File destinationFile = new File(destinationDir, Uri.parse(url).getLastPathSegment());
        request.setDestinationUri(Uri.fromFile(destinationFile));
// You can change the name of the downloads, by changing "download" to everything you want, such as the mWebview title...
        final DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        new Thread("Browser download") {
            public void run() {
                dm.enqueue(request);
            }
        }.start();

        Toast.makeText(MainActivity.this, "Your Downloading start.... ", Toast.LENGTH_SHORT).show();
        // if the link points to an .mp3 resource do something else
    }


    void inflateimage() {

        final Dialog dialogBuilder = new Dialog(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialoglargeimage, null);

        final EditText Websitetitle = (EditText) dialogView.findViewById(R.id.website_title);

        final EditText Websitename = (EditText) dialogView.findViewById(R.id.website_path);
        TextView add_btn = (TextView) dialogView.findViewById(R.id.add_btn);

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String validcheck = checkvaliddata(Websitetitle, Websitename);
                if (validcheck.contains("success")) {
                    Menu m = mNavView.getMenu();
                    m.clear();
                    m.add(Websitetitle.getText().toString());

                    MenuItem mi = m.getItem(m.size() - 1);
                    mi.setTitle(Websitetitle.getText().toString());

                    mi.setIcon(R.drawable.care);
                    mi.setVisible(true);
                    mNavView.inflateMenu(R.menu.activity_main_drawer);
                    Toast.makeText(MainActivity.this, "" + Websitename + "is succefully added", Toast.LENGTH_LONG).show();
                    dialogBuilder.dismiss();
                }

            }
        });

        dialogBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogBuilder.setContentView(dialogView);

        dialogBuilder.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);

        dialogBuilder.show();

    }

    void showhistory() {

        final Dialog dialogBuilder = new Dialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialoghistory, null);
        ListView HistoryList = (ListView) dialogView.findViewById(R.id.history_list);
        ArrayAdapter adapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1, Resource.sSonglist);
        HistoryList.setAdapter(adapter);
        dialogBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogBuilder.setContentView(dialogView);
        dialogBuilder.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialogBuilder.show();


        HistoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // argument position gives the index of item which is clicked
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                String selectedsong = Resource.sSonglist.get(position);
                playsong(selectedsong);

                dialogBuilder.dismiss();
            }
        });

    }

    String checkvaliddata(EditText websitetitle, EditText websiteurl) {

        if (websitetitle.getText().toString().equals("")) {

            websitetitle.setError(" Enter Your email Address");
            websitetitle.requestFocus();
            return "Please Provide Email address";
        }


        if (websiteurl.getText().toString().equals("")) {

            websiteurl.setError("Enter Url path ");
            websiteurl.requestFocus();
            return "Please Provide password";
        }

        if (Patterns.WEB_URL.matcher(websiteurl.getText().toString()).matches()) {

            websiteurl.setError("path is wrong or invalid");
            websiteurl.requestFocus();
            return "Please Provide password";
        }


        return "success";
    }


    public void playsong(final String url) {
        mProgress.setVisibility(View.VISIBLE);
        mPause.setVisibility(View.GONE);
        new Thread("Browser download") {


            public void run() {

                player.reset();
                try {
                    player.setDataSource(url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    player.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                            @Override
                            public void onBufferingUpdate(MediaPlayer mp, int percent) {

                                mProgress.setVisibility(View.GONE);
                                mPause.setVisibility(View.VISIBLE);
                            }

                        });

                        player.start();
                        //   sNotificatinImage.setImageBitmap(nIMAGE);
                    }
                });

            }
        }.start();

    }

}
